/*Code Copyright 2017, 2020 J.P.Reilly
* This file is part of WordSoup.
*
* WordSoup is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordSoup is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordSoup.  If not, see <https://www.gnu.org/licenses/>.
*/
 
/**Exercise: Final Project
* Author: Joseph Reilly
* Project Purpose: Solitaire Scrabble-like word game 
* Input:  user clicks on buttns (letter tiles, enter/clear buttons, menu items)
* Desired Output:  confirmation/deny word matches, high scores, help menu
* Variables and Classes:  Custom: WordList, LetterBag, ScoreManager. Standard: Int, Char, String, Label, Button, BorderPane, GridPane, VBox, Pane,
* Arc, Ellipse, Rectangle, MenuBar, Menu, MenuItem, RadioMenuItem, ToggleGroup, File, Scanner, ArrayList<>, boolean, String[], List<>
* Formulas:  N/A
* Description of the main algorithm: Load main interface, get letter tile clicks from user, compare user entered word with word list on enter press,
* if word is in the word list discard word from list, discard letters from tiles, get new letters from letter bag for letter tiles, update score board info. 
* If user word isn't on the list, notify user, re-enable used letter tiles, clear user word field.
* Date: 11/27/17-12/11/17
* Copyright 2017 Joseph Reilly
**/

package WordSoup;

import javafx.event.*;
import javafx.application.*;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.effect.*;


public class MainSoup extends Application {
	//number of tiles
	final int TILE_NUM = 16;	
	
	//layout controls
	private BorderPane BorderPaneOne;
	private GridPane GridPaneOne, ScoreBoard, LettersInPlay;
	private VBox SideButtonBox;
	private Pane SoupPane;
	
	//background drawing stuff
	private Arc SbArcOne, SpArcThree;
	private Ellipse SbEllOne, SbEllTwo, SbEllThree, SpEllFour, SpEllFive;
	private Rectangle SpRecOne;
	
	//output labels
	private Label lblOutput, lblLetterBagLabel, lblLetterBagValue, lblScoreLabel,  lblScoreValue;
	private Label lblUserWord;
	
	//letter tiles and buttons
	private Button[] btnLetterTiles;
	private Button btnEnter, btnClear;
	
	//menu bar stuff
	private MenuBar MenuBarOne;
	private Menu mnuFile, mnuOptions, mnuLetterBag, mnuScores, mnuLanguage, mnuHelp;
	private MenuItem  mniNewGame, mniExit, mniShuffle, mniViewScores, mniSaveScore, mniAbout;
	private RadioMenuItem mnrSmall, mnrMedium, mnrLarge, mnrEnglish, mnrSpanish;
	private ToggleGroup BagSize, LangGroup;

	//language files, english, spanish
	private String En_WordFile = "en_wordfile.txt";
	private String En_SmallBag = "en_smlbag.txt";
	private String En_MediumBag = "en_medbag.txt";
	private String En_LargeBag = "en_lrgbag.txt";
	private String Es_WordFile = "es_wordfile.txt";
	private String Es_SmallBag = "es_smlbag.txt";
	private String Es_MediumBag = "es_medbag.txt";
	private String Es_LargeBag = "es_lrgbag.txt";
	
	//default language and letterbag size settings
	private String CurrentWordFile = En_WordFile;
	private String CurrentSmall = En_SmallBag;
	private String CurrentMedium = En_MediumBag;
	private String CurrentLarge = En_LargeBag;
	private int CurrentLetterBagSize = 3;
	
	//custom classes
	private WordList WordListOne = new WordList(CurrentWordFile);
	private LetterBag LetterBagOne = new LetterBag(CurrentLetterBagSize, CurrentLarge, CurrentMedium, CurrentSmall);
	private ScoreManager ScoreManagerOne = new ScoreManager();
	
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage PrimaryStage) {
		//setup visual controls
		//creates IO labels/fields
		CreateLabels();
		
		//creates letter buttons
		CreateLetterButtons();
		
		//creates score board
		CreateScoreBoard();
		
		//creates menu bar/items, plus default menu settings based on WordList/LetterBag creation above
		CreateMenu();
		mnrLarge.setSelected(true);
		mnrEnglish.setSelected(true);
		
		//creates button box and buttons
		CreateButtonBox();
		
		//background image
		CreateBackground();
		
		//grid pane for main control layout
		GridPaneOne = new GridPane();
		GridPaneOne.add(lblOutput, 1, 1);
		GridPaneOne.add(lblUserWord, 1, 2);
		GridPaneOne.add(ScoreBoard, 2, 1, 1, 2);
		GridPaneOne.add(SoupPane, 1, 3);
		GridPaneOne.add(LettersInPlay, 1, 3);
		GridPaneOne.add(SideButtonBox, 2, 3);
		GridPaneOne.setAlignment(Pos.CENTER);
		GridPaneOne.setPadding(new Insets(1));
		GridPaneOne.setVgap(5);
		GridPaneOne.setHgap(15);
		
		//setup  border pane to hold main grid pane and menu bar
		BorderPaneOne = new BorderPane();
		BorderPaneOne.setCenter(GridPaneOne);
		BorderPaneOne.setTop(MenuBarOne);
		
		//setup first round of letters on the tiles and set letters left label
		for(int i = 0; i < TILE_NUM; i++) {
			btnLetterTiles[i].setText(String.valueOf(LetterBagOne.getPlayLetter()));
		}
		lblLetterBagValue.setText(String.valueOf(LetterBagOne.lettersLeft()));
		
		//button  event handlers
		btnEnter.setOnAction(new btnEnter_onClick());
		btnClear.setOnAction(new btnClear_onClick());
		
		//menu event handlers
		
		mniNewGame.setOnAction(new mniNewGame_onClick());
		mniExit.setOnAction(new mniExit_onClick());
		mniShuffle.setOnAction(new mniShuffle_onClick());
		mnrSmall.setOnAction(new BagSize_changed());
		mnrMedium.setOnAction(new BagSize_changed());
		mnrLarge.setOnAction(new BagSize_changed());
		mniViewScores.setOnAction(new mniViewScores_onClick(PrimaryStage));
		mniSaveScore.setOnAction(new mniSaveScore_onClick(PrimaryStage));
		mniAbout.setOnAction(new mniAbout_onClick(PrimaryStage));
		mnrEnglish.setOnAction(new Language_changed());
		mnrSpanish.setOnAction(new Language_changed());
		
		
		//letter button event handlers
		for (int i = 0; i < TILE_NUM; i++) {
			btnLetterTiles[i].setOnAction(new btnLetterTiles_onClick(i));
		}
		
		//set the scene, stage, and on with the show
		PrimaryStage.setTitle("Word Soup");
		Scene PrimaryScene = new Scene(BorderPaneOne, 485, 400);
		PrimaryScene.getStylesheets().add("styles.css");
        
        //PrimaryStage.setResizable(false); doesnt seem to work on OpenSuse circa May 2020
        
        //disable window resizing the hard way
		PrimaryStage.setMinWidth(485);
		PrimaryStage.setMinHeight(400);
		PrimaryStage.setMaxWidth(485);
		PrimaryStage.setMaxHeight(400);
		
		PrimaryStage.setScene(PrimaryScene);
		PrimaryStage.show();
	}
	
	
	class mniNewGame_onClick implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			
			//reset labels, generate new word list and letter bag
			lblLetterBagValue.setText("99");
			lblScoreValue.setText("0");
			lblOutput.setText("");
			WordListOne = new WordList(CurrentWordFile);
			LetterBagOne = new LetterBag(CurrentLetterBagSize, CurrentLarge, CurrentMedium, CurrentSmall);
			
			//get new tiles and set letters left label
			for(int i = 0; i < TILE_NUM; i++) {
				btnLetterTiles[i].setText(String.valueOf(LetterBagOne.getPlayLetter()));
			}
			
			lblLetterBagValue.setText(String.valueOf(LetterBagOne.lettersLeft()));
		}
	}
	
	class BagSize_changed implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			
			if(mnrSmall.isSelected()) {
				CurrentLetterBagSize = 1;
			}
			else if(mnrMedium.isSelected()) {
				CurrentLetterBagSize = 2;
			}
			else {
				CurrentLetterBagSize = 3;
			}
			
			//reset labels, generate new word list and letter bag using pragmatic click of the new game menu item
			mniNewGame.fire();
		}
	}
	
	class Language_changed implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			
			if(mnrSpanish.isSelected()) {
				CurrentWordFile = Es_WordFile;
				CurrentSmall = Es_SmallBag;
				CurrentMedium = Es_MediumBag;
				CurrentLarge = Es_LargeBag;
			}
			else {
				CurrentWordFile = En_WordFile;
				CurrentSmall = En_SmallBag;
				CurrentMedium = En_MediumBag;
				CurrentLarge = En_LargeBag;
			}
			//reset labels, generate new word list and letter bag using pragmatic click of the new game menu item
			mniNewGame.fire();
		}
	}
	
	
	class btnLetterTiles_onClick implements EventHandler<ActionEvent>{
		int Index = 0;
		
		//constructor, gets button index number so we know which letter button we're dealing with
		public btnLetterTiles_onClick(int Index) {
			this.Index = Index;
		}
		
		@Override
		public void handle(ActionEvent Event) {
			//user can only use each button once so disable the button after click
			btnLetterTiles[Index].setDisable(true);
			lblUserWord.setText(lblUserWord.getText() + btnLetterTiles[Index].getText());
			lblOutput.setText("");
		}
	}
	
	
	class btnEnter_onClick  implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			int Score = 0;
			
			//check for user word on the word list, if there's a match, get new letters for the used tiles/letters
			if(WordListOne.checkList(lblUserWord.getText())) {
				for(int i = 0; i < TILE_NUM; i++) {
					if(btnLetterTiles[i].isDisabled()) {
						btnLetterTiles[i].setDisable(false);
						btnLetterTiles[i].setText(String.valueOf(LetterBagOne.getPlayLetter()));
					}
				}
				
				
				//figure out score
				if(lblUserWord.getText().length() >= 14) {
					Score +=  lblUserWord.getText().length()+ 12;
				}
				else if(lblUserWord.getText().length() >= 12) {
					Score +=  lblUserWord.getText().length() + 6;
				}
				else if(lblUserWord.getText().length() >= 8) {
					Score +=  lblUserWord.getText().length() + 4;
				}
				else if(lblUserWord.getText().length() >= 4) {
					Score +=  lblUserWord.getText().length() + 2;
				}
				else {
					Score +=  lblUserWord.getText().length();
				}
					
				lblScoreValue.setText(String.valueOf(Integer.parseInt(lblScoreValue.getText()) + Score));
				lblUserWord.setText("");
				
				//Decrement letter bag count
				lblLetterBagValue.setText(String.valueOf(LetterBagOne.lettersLeft()));
				if(Integer.parseInt(lblLetterBagValue.getText()) == 0) {
					lblOutput.setText("You won! Your last word score was: " + Score);
				}
				else {
					lblOutput.setText("Word found! You scored: " + Score);
				}
			}
			else {
				lblOutput.setText("There was no match, try again...");
				lblUserWord.setText("");
			}
			
			//re-enable all buttons
			for(int i = 0; i < TILE_NUM; i++) {
				btnLetterTiles[i].setDisable(false);
			}
		}
	}
	
	class mniExit_onClick  implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			System.exit(0);
		}
	}
	
	class btnClear_onClick  implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			//clear the user word text field, re-enable all buttons
			lblUserWord.setText("");
			for(int i = 0; i < TILE_NUM; i++) {
				btnLetterTiles[i].setDisable(false);
			}
		}
	}
	
	class mniShuffle_onClick implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent Event) {
			if(lblUserWord.getText() == "") {
				for(int i = 0; i < TILE_NUM; i++) {
					//return letters to letter bag, get new letters
					LetterBagOne.returnPlayLetter(btnLetterTiles[i].getText().charAt(0));
					btnLetterTiles[i].setText(String.valueOf(LetterBagOne.getPlayLetter()));
				}
			}
		}
	}
	
	class mniSaveScore_onClick implements EventHandler<ActionEvent>{
		Stage PrimaryStage;
		
		public mniSaveScore_onClick(Stage PrimaryStage) {
			this.PrimaryStage = PrimaryStage;
		}
		
		@Override
		public void handle(ActionEvent Event) {
			//set the secondary scene and stage, modal dialog
			TextField txtName = new TextField();
			Button btnEnter = new Button("Enter");
			HBox NameBox = new HBox(txtName, btnEnter);
			Stage SecondaryStage = new Stage();
			Scene SecondaryScene = new Scene(NameBox, 300, 50);
			
			NameBox.setAlignment(Pos.CENTER);
			
			btnEnter.setOnAction(event -> {
				ScoreManagerOne.addItem(txtName.getText(), lblScoreValue.getText());
				SecondaryStage.close();
			});
			
		
			SecondaryStage.setTitle("Please enter your name");
			SecondaryScene.getStylesheets().add("styles.css");
			SecondaryStage.initOwner(PrimaryStage);
			SecondaryStage.initModality(Modality.WINDOW_MODAL); 
			//SecondaryStage.setResizable(false); doesnt seem to work on OpenSuse circa May 2020
			//disable window resizing the hard way
            SecondaryStage.setMinWidth(300);
            SecondaryStage.setMinHeight(50);
            SecondaryStage.setMaxWidth(300);
            SecondaryStage.setMaxHeight(50);
			SecondaryStage.setScene(SecondaryScene);
			SecondaryStage.showAndWait();
			
		
		}
	}
	
	class mniViewScores_onClick implements EventHandler<ActionEvent>{
		Stage PrimaryStage;
		
		public mniViewScores_onClick(Stage PrimaryStage) {
			this.PrimaryStage = PrimaryStage;
		}
		@Override
		public void handle(ActionEvent Event) {
			//set the secondary scene and stage, modal dialog
			ListView<String> ListViewOne = new ListView<>();
			Stage SecondaryStage = new Stage();
			Scene SecondaryScene = new Scene(ListViewOne, 200, 300);
			
			ListViewOne.getItems().addAll(ScoreManagerOne.getItemList());
			
			SecondaryStage.setTitle("Top 10 Scores");
			SecondaryScene.getStylesheets().add("styles.css");
			SecondaryStage.initOwner(PrimaryStage);
			SecondaryStage.initModality(Modality.WINDOW_MODAL); 
			//SecondaryStage.setResizable(false); doesnt seem to work on OpenSuse circa May 2020
			SecondaryStage.setMinWidth(200);
            SecondaryStage.setMinHeight(300);
            SecondaryStage.setMaxWidth(200);
            SecondaryStage.setMaxHeight(300);
			SecondaryStage.setScene(SecondaryScene);
			SecondaryStage.showAndWait();
		}
	}

	class mniAbout_onClick implements EventHandler<ActionEvent>{
		Stage PrimaryStage;
		
		public mniAbout_onClick(Stage PrimaryStage) {
			this.PrimaryStage = PrimaryStage;
		}
		
		@Override
		public void handle(ActionEvent Event) {
			//set the secondary scene and stage, modal dialog
			Label Help = new Label();
			Scene SecondaryScene = new Scene(Help, 400, 580);
			Stage SecondaryStage = new Stage();
			
			Help.setText("Word Soup\n\nGame Play:\nSelect letter tiles to make words and click enter to score.\nWhen the bag of letters runs out, you win!" + 
				"\n\nScoring: 1 point per letter plus:\n" + 
				"\t2 points for words of 4-7 letters\n" + 
				"\t4 points for words of 8-11 letters\n" + 
				"\t6 points for words of 12-13 letters\n" + 
				"\t12 points for words of 14-16 letters" + 
				"\n\nFile Menu:\nStart a new game or exit." +
				"\n\nOptions Menu:\nAdjust the letter bag size and shuffle the letters\nif you get stuck." + 
				"\n\nScores Menu:\nView or save your score. Only the top 10 scores are saved.\n You can save your score at any time." + 
				"\n\nTip:\nAfter winning, you can continue until there's no more letters\nto get a better score." +
				"\n\nCopyright 2017 Joseph Reilly");
			
			Help.setAlignment(Pos.CENTER);
		
			SecondaryStage.setTitle("About");
			SecondaryScene.getStylesheets().add("styles.css");
			SecondaryStage.initOwner(PrimaryStage);
			SecondaryStage.initModality(Modality.WINDOW_MODAL); 
			//SecondaryStage.setResizable(false); doesnt seem to work on OpenSuse circa May 2020
			SecondaryStage.setMinWidth(400);
            SecondaryStage.setMinHeight(580);
            SecondaryStage.setMaxWidth(400);
            SecondaryStage.setMaxHeight(580);
			SecondaryStage.setScene(SecondaryScene);
			SecondaryStage.showAndWait();
			
		
		}
	}
	
	private void CreateLetterButtons() {
		//grid pane for letter button layout
		LettersInPlay = new GridPane();
		btnLetterTiles = new Button[TILE_NUM];
		//load the buttons up with first round of letters
		for(int i = 0; i < TILE_NUM; i++) {
			btnLetterTiles[i] = new Button();
			btnLetterTiles[i].getStyleClass().add("letter-button");
			btnLetterTiles[i].setPrefSize(40, 40);
		}
		
		LettersInPlay.add(btnLetterTiles[0], 1, 1);
		LettersInPlay.add(btnLetterTiles[1], 3, 1);
		LettersInPlay.add(btnLetterTiles[2], 5, 1);
		LettersInPlay.add(btnLetterTiles[3], 7, 1);
		LettersInPlay.add(btnLetterTiles[4], 2, 2);
		LettersInPlay.add(btnLetterTiles[5], 4, 2);
		LettersInPlay.add(btnLetterTiles[6], 6, 2);
		LettersInPlay.add(btnLetterTiles[7], 1, 3);
		LettersInPlay.add(btnLetterTiles[8], 3, 3);
		LettersInPlay.add(btnLetterTiles[9], 5, 3);
		LettersInPlay.add(btnLetterTiles[10], 7, 3);
		LettersInPlay.add(btnLetterTiles[11], 2, 4);
		LettersInPlay.add(btnLetterTiles[12], 4, 4);
		LettersInPlay.add(btnLetterTiles[13], 6, 4);
		LettersInPlay.add(btnLetterTiles[14], 3, 5);
		LettersInPlay.add(btnLetterTiles[15], 5, 5);
		LettersInPlay.setTranslateY(25);
		LettersInPlay.setTranslateX(5);
	}
	
	private void CreateLabels() {
		//setup text field
		lblUserWord = new Label();
		lblUserWord.getStyleClass().add("label-one");
		lblUserWord.setMinWidth(300.0);
		lblUserWord.setMinHeight(30.0);
		lblUserWord.setAlignment(Pos.CENTER);
		GridPane.setHalignment(lblUserWord, HPos.CENTER);
		
		//setup output labels
		lblOutput = new Label();
		lblOutput.getStyleClass().add("label-one");
		lblOutput.setMinWidth(300.0);
		lblOutput.setMinHeight(30.0);
		lblOutput.setAlignment(Pos.CENTER);
		GridPane.setHalignment(lblOutput, HPos.CENTER);
		GridPane.setValignment(lblOutput, VPos.TOP);
	}

	private void CreateButtonBox() {
		//setup button box and buttons
		btnEnter = new Button("Enter");
		btnClear = new Button("Clear");
		SideButtonBox = new VBox(btnEnter, btnClear);
		SideButtonBox.setAlignment(Pos.CENTER);
		SideButtonBox.setSpacing(25.0);
	}
	
	private void CreateScoreBoard() {
		//grid pane and labels for score board layout
		ScoreBoard = new GridPane();
		lblLetterBagLabel = new Label("Letter Bag: ");
		lblLetterBagValue = new Label("99");
		lblScoreLabel = new Label("Score: ");
		lblScoreValue = new Label("0");
		ScoreBoard.setPrefWidth(135.00);
		ScoreBoard.add(lblLetterBagLabel, 1, 1);
		ScoreBoard.add(lblLetterBagValue, 2, 1);
		ScoreBoard.add(lblScoreLabel, 1, 2);
		ScoreBoard.add(lblScoreValue, 2, 2);
		ScoreBoard.setPadding(new Insets(2));
		ScoreBoard.setId("scores");
		lblLetterBagLabel.setId("scores-label");
		lblLetterBagValue.setId("scores-text");
		lblScoreLabel.setId("scores-label");
		lblScoreValue.setId("scores-text");
		GridPane.setHalignment(lblScoreValue, HPos.CENTER);
		GridPane.setHalignment(lblLetterBagValue, HPos.CENTER);
	}
	
	private void CreateMenu() {
		//file menu
		mnuFile = new Menu("File");
		mniNewGame = new MenuItem("New Game");
		mniExit = new MenuItem("Exit");
		mnuFile.getItems().addAll(mniNewGame, mniExit);
		
		//options menu
		mnuOptions = new Menu("Options");
		mniShuffle = new MenuItem("Shuffle");
		mnuLetterBag = new Menu("Letter Bag");
		mnrSmall = new RadioMenuItem("Small");
		mnrMedium = new RadioMenuItem("Medium");
		mnrLarge= new RadioMenuItem("Large");
		BagSize = new ToggleGroup(); //set toggle group for letter bag size
		mnrSmall.setToggleGroup(BagSize);
		mnrMedium.setToggleGroup(BagSize);
		mnrLarge.setToggleGroup(BagSize);
		mnuLetterBag.getItems().addAll(mnrSmall, mnrMedium, mnrLarge);
		mnuOptions.getItems().addAll(mniShuffle, new SeparatorMenuItem(), mnuLetterBag);
		
		//scores menu
		mnuScores = new Menu("Scores");
		mniViewScores = new MenuItem("View Scores");
		mniSaveScore = new MenuItem("Save Score");
		mnuScores.getItems().addAll(mniViewScores, mniSaveScore);
		
		//language menu
		mnuLanguage = new Menu("Language");
		mnrEnglish = new RadioMenuItem("English");
		mnrSpanish = new RadioMenuItem("Spanish");
		LangGroup = new ToggleGroup();
		mnrEnglish.setToggleGroup(LangGroup);
		mnrSpanish.setToggleGroup(LangGroup);
		mnuLanguage.getItems().addAll(mnrEnglish, mnrSpanish);
		
		//help menu
		mnuHelp = new Menu("Help");
		mniAbout = new MenuItem("About");
		mnuHelp.getItems().add(mniAbout);
		
		//setup main menu bar
		MenuBarOne = new MenuBar();
		MenuBarOne.getMenus().add(mnuFile);
		MenuBarOne.getMenus().add(mnuOptions);
		MenuBarOne.getMenus().add(mnuScores);
		MenuBarOne.getMenus().add(mnuLanguage);
		MenuBarOne.getMenus().add(mnuHelp);
	}

	private void CreateBackground() {
		SoupPane = new Pane();
		
		//the outer bowl
		Stop[] BowlStops = new Stop[] { new Stop(0, Color.DARKSLATEGRAY), new Stop(1, Color.TEAL)};
		LinearGradient BowlPaint = new LinearGradient(0, 0, .5, 0, true, CycleMethod.REFLECT, BowlStops);
		SbArcOne = new Arc(144.0, 100.0, 135.0, 120.0, 180.0, 180.0);
		SbArcOne.setFill(BowlPaint);
		
		//the rim of the bowl
		SbEllOne = new Ellipse(144.0, 100.0, 133.0, 48.0);
		SbEllOne.setStroke(Color.BLACK);
		SbEllOne.setStrokeWidth(6);
		SbEllOne.setFill(null);
		
		//the inner bowl
		SbEllTwo = new Ellipse(144.0, 100.0, 130.0, 45.0);
		SbEllTwo.setFill(BowlPaint);
		
		//the soup
		Stop[] SoupStops = new Stop[] { new Stop(0, Color.DARKORANGE), new Stop(1, Color.ORANGE)};
		RadialGradient SoupPaint = new RadialGradient(0, 0, .5, .75, .5, true, CycleMethod.NO_CYCLE, SoupStops);
		SbEllThree = new Ellipse(144.0, 112.0, 128.0, 36.0);
		SbEllThree.setFill(SoupPaint);
		
		//soup spoon handle
		Stop[] SpoonStops = new Stop[] { new Stop(0, Color.SILVER), new Stop(1, Color.DARKSLATEGRAY)};
		LinearGradient SpoonPaint = new LinearGradient(0, 0, .75, 0, true, CycleMethod.REFLECT, SpoonStops);
		SpRecOne = new Rectangle(268.0, 8.0, 20.0, 100.0);
		SpRecOne.setRotate(70.0);
		SpRecOne.setFill(SpoonPaint);
		
		
		//soup spoon bowl outer
		Stop[] SpoonBowlStops = new Stop[] {new Stop(0, Color.DARKSLATEGRAY), new Stop(1, Color.SILVER)};
		RadialGradient SpoonBowlPaint = new RadialGradient(0, 0, .5, .8, .8, true, CycleMethod.NO_CYCLE, SpoonBowlStops);
		SpArcThree = new Arc(190.0, 85.0, 50.0, 15.0, 180.0, 180.0);
		SpArcThree.setRotate(350.0);
		SpArcThree.setFill(SpoonBowlPaint);
		
		//soup spoon bowl inner
		SpEllFour = new Ellipse(185.0, 80.0, 50.0, 15.0);
		SpEllFour.setRotate(350.0);
		SpEllFour.setFill(SpoonBowlPaint);
		
		//soup in spoon
		SpEllFive = new Ellipse(185.0, 80.0, 40.0, 10.0);
		SpEllFive.setRotate(350.0);
		SpEllFive.setFill(SoupPaint);
	
		SbArcOne.setOpacity(.90);
		SbEllTwo.setOpacity(.90);
		SbEllThree.setOpacity(.90);
		SbEllOne.setOpacity(.90);
		SpArcThree.setOpacity(.90);
		SpEllFour.setOpacity(.90);
		SpEllFive.setOpacity(.90); 
		SpRecOne.setOpacity(.95);
		
		//fancy effects to make the soup look more picture like
		Glow EffectGlow = new Glow();
		DropShadow EffectShadow= new DropShadow();
		EffectShadow.setOffsetX(5.0);
		EffectShadow.setOffsetY(20.0);
		EffectShadow.setColor(Color.GRAY);
		EffectShadow.setInput(EffectGlow);
		
	
		SbArcOne.setEffect(EffectShadow);
		SbEllTwo.setEffect(EffectGlow);
		SbEllThree.setEffect(EffectGlow);
		SbEllOne.setEffect(EffectGlow);
		SpArcThree.setEffect(EffectShadow);
		SpEllFour.setEffect(EffectGlow);
		SpEllFive.setEffect(EffectGlow);
		SpRecOne.setEffect(EffectGlow);
		SoupPane.getChildren().addAll(SbArcOne,  SbEllTwo, SbEllThree, SbEllOne, SpArcThree, SpEllFour, SpEllFive, SpRecOne);
	}
}


