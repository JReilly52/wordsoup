/*Code Copyright 2017, 2020 J.P.Reilly
* This file is part of WordSoup.
*
* WordSoup is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordSoup is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordSoup.  If not, see <https://www.gnu.org/licenses/>.
*/

package WordSoup;

import java.util.*;

class ScoreManager extends FileWrangler {
	//fields, states
	final String FILENAME = "scores.txt";
	private String StringType = "score list"; //type of string list the file contains
	
	public ScoreManager() {
		this.readItemList(StringType, FILENAME);
	}
	
	public String getItem(int Index) {
		String ReturnValue = "";
		
		if(Index < ItemList.size()) {
			ReturnValue = ItemList.get(Index);
		}
		
		return ReturnValue;
	}
	
	public List<String> getItemList(){
		return Collections.unmodifiableList(ItemList);
	}
	
	public void addItem(String Name, String Score) {
		ItemList.add(Name + " - " + Score);
		
		//sort and trim the scores list
		this.sortList();
		
		this.saveScore(StringType, FILENAME);
	}
	
	public void saveScore(String StringType, String FileName) {
		super.writeItemList(StringType, FileName);
	}
	
	private void sortList() {
		ArrayList<String> Names = new ArrayList<>();
		ArrayList<Integer> Scores = new ArrayList<>();
		String[] Items;
		
		for(int i = 0; i < ItemList.size(); i++) {
			Items = ItemList.get(i).split(" - ");
			Names.add(Items[0]);
			Scores.add(Integer.parseInt(Items[1]));
		}

		//selection sort
		int HighestValue, HighestIndex;
		String NameValue;
		
		for (int StartIndex = 0; StartIndex < Scores.size()-1; StartIndex++) {
			HighestIndex = StartIndex;
			HighestValue = Scores.get(StartIndex);
			NameValue = Names.get(StartIndex);
			
			for(int Index = StartIndex + 1; Index <  Scores.size(); Index++) {
				if(Scores.get(Index) > HighestValue) {
					HighestValue = Scores.get(Index);
					NameValue = Names.get(Index);
					HighestIndex = Index;
				}
			}
			//swap highest score and parallel name element 
			Scores.set(HighestIndex, Scores.get(StartIndex));
			Names.set(HighestIndex, Names.get(StartIndex));
			Scores.set(StartIndex, HighestValue);
			Names.set(StartIndex, NameValue);
		}
		
		ItemList.clear();
		
		for(int i = 0; i < Names.size(); i++) {
			ItemList.add(Names.get(i) + " - " + Scores.get(i));
		}

		while(ItemList.size() > 10) {
			ItemList.remove(10);
		}
	}
}
