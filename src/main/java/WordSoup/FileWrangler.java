/*Code Copyright 2017, 2020 J.P.Reilly
* This file is part of WordSoup.
*
* WordSoup is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordSoup is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordSoup.  If not, see <https://www.gnu.org/licenses/>.
*/

package WordSoup;

import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import javafx.stage.FileChooser;

abstract class FileWrangler {
	private boolean Loop = true;
	protected ArrayList<String> ItemList = new ArrayList<>();
	
	//abstract class to read/write strings to a file in plain text
	protected void readItemList(String StringType, String FileName) {
		Loop = true;
		
		//load strings from file into array list
		do {
			try {
                //getResource() added May 2020 to deal with issues finding resource files when using gradle, more study is needed
                File FileResource = new File(getClass().getClassLoader().getResource(FileName).getFile()
);
				Scanner StringFile = new Scanner(FileResource);
				//keep loading if the file has more
				while(StringFile.hasNext()) {
					ItemList.add(StringFile.nextLine());
				}
				StringFile.close();
				//if you make it to here without error, cancel the loop
				Loop = false;
			}
			catch (Exception e) {
				//if default file is missing, prompt user for an alternate file
				System.out.println("<<<Error: There was a problem with the " + StringType + " file>>>\n<<< System Message: " + e.getMessage() + " >>>");
				if(JOptionPane.showConfirmDialog(null, StringType + " file not found, Would you like to select a file manually?",  "<<<Error>>>", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
					System.exit(0);
				}
				//get alternate file name
				FileName = new FileChooser().showOpenDialog(null).toString();
			}
		} while (Loop);
	}
	
	protected void writeItemList(String StringType, String FileName) {
		Loop = true;
		
		do {
			try {
                //getResource() added May 2020 to deal with issues finding resource files when using gradle, more study is needed
                File FileResource = new File(getClass().getClassLoader().getResource(FileName).getFile()
);
				//write array list strings to file, create or overwrite only
				PrintWriter StringFile = new PrintWriter(FileResource);
				for(int i = 0; i < ItemList.size(); i++) {
					StringFile.println(ItemList.get(i));
				}
				StringFile.close();
				Loop = false;
			}
			catch (Exception e) {
				//if default word file is missing, prompt user for an alternate file
				System.out.println("<<<Error: There was a problem with the " + StringType + " file>>>\n<<< System Message: " + e.getMessage() + " >>>");
				JOptionPane.showMessageDialog(null, StringType + " file write error, we can't fix this, bye...",  "<<<Error>>>", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
				
			}
		} while (Loop);
	}
	
	
}
