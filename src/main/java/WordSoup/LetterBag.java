/*Code Copyright 2017, 2020 J.P.Reilly
* This file is part of WordSoup.
*
* WordSoup is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordSoup is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordSoup.  If not, see <https://www.gnu.org/licenses/>.
*/

package WordSoup;

import java.util.Random;

class LetterBag extends FileWrangler {
	//fields, states
	//sm/med/lrg word list files, must be assigned by main class
	private String Small; 
	private String Medium;
	private String Large;
	private String StringType = "letter list"; //type of string list the file contains
	private int LetterBagDepth;
	
	//constructors
	public LetterBag(int LetterBagDepth, String Large, String Medium, String Small) {
		this.LetterBagDepth = LetterBagDepth;
		this.Small = Small;
		this.Medium = Medium;
		this.Large = Large; 
				
		this.generateLetterBag();
	}
	
	//methods, actions
	public int lettersLeft() {
		return super.ItemList.size();
	}
	
	public void generateLetterBag() {
		super.ItemList.clear();
		
		
		//select letter bag size
		if(LetterBagDepth == 1) {
			super.readItemList(StringType, Small);
		}
		else if (LetterBagDepth == 2) {
			super.readItemList(StringType, Medium);			
		}
		else {
			super.readItemList(StringType, Large);
		}
			
	}
	
	//getters/setters, actions
	public char getPlayLetter() {
		Random RandomNumbers = new Random();
		int Index = 0;
		char ReturnValue = '&';
		
		//return a letter from the letter bag, unless it's empty, then return &
		if(! super.ItemList.isEmpty()) {
			Index = RandomNumbers.nextInt( super.ItemList.size());
			ReturnValue =  super.ItemList.get(Index).charAt(0);
			super.ItemList.remove(Index);
		}
		
		return ReturnValue;
	}
	
	public void returnPlayLetter(char Letter) {
		 super.ItemList.add(String.valueOf(Letter));
	}
	
}
