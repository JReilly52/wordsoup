/*Code Copyright 2017, 2020 J.P.Reilly
* This file is part of WordSoup.
*
* WordSoup is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordSoup is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordSoup.  If not, see <https://www.gnu.org/licenses/>.
*/

package WordSoup;

class WordList extends FileWrangler{
	//fields, states
	private String FileName;
	private String StringType = "word list"; //type of string list the file contains
	
	//constructors
	public WordList(String FileName) {
		this.FileName = FileName;
		this.getWordList();
	}
	
	//methods
	public boolean checkList(String Word) {
		boolean Found = false;
		int ListSize = super.ItemList.size()-1;
		int Index = 0;
		
		while(!Found && (Index < ListSize)) {
			if(Word.toLowerCase().equals(super.ItemList.get(Index).toLowerCase())) {
				Found = true;
				super.ItemList.remove(Index);
			}
			Index++;
		}
		
		return Found;	
	}

	//getters
	private void getWordList() {
		//load the word file using super class method
		super.readItemList(StringType, FileName);
	}

}
